PostgreSQL 9.5 Management Container
===================================

Description
-----------

A container used to manage a PostgreSQL instance (=container).

It is based on the [official PostgreSQL container](https://hub.docker.com/_/postgres/) as my [postgres container](https://hub.docker.com/r/sebcworks/postgres/).

It only install postgresql-client (and doc) and some small tools: nano, ftp and telnet.

It was made to be used with my other postgres containers: [sebcworks/postgres](https://hub.docker.com/r/sebcworks/postgres/) and [sebcworks/postgres-data](https://hub.docker.com/r/sebcworks/postgres-data/).

Usage
-----

It is pretty straightforward:

### PostgreSQL installation (with data container)
    
    docker pull sebcworks/postgres-data
    docker pull sebcworks/postgres
    docker run --name pgdata sebcworks/postgres-data
    
    docker run --name pgdb -e POSTGRES_PASSWORD=tobechanged --volumes-from pgdata -d sebcworks/postgres
    
    docker exec -it pgdb bash
    su postgres
    psql
    ALTER USER postgres WITH ENCRYPTED PASSWORD 'mysecurepassword';
    
### Manager installation
    
    docker pull sebcworks/postgres-manager
    docker run --rm --name pgman-tmp --link pgdb:postgres --volumes-from pgdata -it sebcworks/postgres-manager
    
    gosu postgres psql -h postgres
    
